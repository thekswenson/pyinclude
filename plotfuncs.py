"""
Functions for plotting data.
"""

import copy
import textwrap
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import itertools

from typing import List




#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#Constants:


def heatmap(data: np.ndarray, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
  """
  Create a heatmap from a numpy array and two lists of labels.

  Notes
  -----
      Copied from the matplotlib gallery.

  Parameters
  ----------
  data
      A 2D numpy array of shape (M, N).
  row_labels
      A list or array of length M with the labels for the rows.
  col_labels
      A list or array of length N with the labels for the columns.
  ax
      A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
      not provided, use current axes or create a new one.  Optional.
  cbar_kw
      A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
  cbarlabel
      The label for the colorbar.  Optional.
  **kwargs
      All other arguments are forwarded to `imshow`.

  Notes
  -----
      Some of the values for kwargs are:
      - vmax: the maximum value a matrix element can take
      - vmin: the minimum value a matrix element can take
  """

  if not ax:
      ax = plt.gca()

  # Plot the heatmap
  current_cmap = copy.copy(matplotlib.cm.get_cmap())    #type: ignore
  current_cmap.set_bad(color='white')
  im = ax.imshow(data, **kwargs)

  # Create colorbar
  cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
  cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

  # Show all ticks and label them with the respective list entries.
  #ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
  #ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)
  ax.set_xticks(np.arange(data.shape[1]))
  ax.set_yticks(np.arange(data.shape[0]))
  ax.set_xticklabels(labels=col_labels)
  ax.set_yticklabels(labels=row_labels)

  # Let the horizontal axes labeling appear on top.
  ax.tick_params(top=True, bottom=False,
                 labeltop=True, labelbottom=False)

  # Rotate the tick labels and set their alignment.
  plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
           rotation_mode="anchor")

  # Turn spines off and create white grid.
  #ax.spines[:].set_visible(False)

  ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
  ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
  ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
  ax.tick_params(which="minor", bottom=False, left=False)

  return im, cbar


def annotate_heatmap(im, data: np.ndarray=None, valfmt="{x:.2f}",
                     textcolors=("white", "black"),
                     threshold=None, **textkw) -> List[str]:
  """
  A function to annotate a heatmap.

  Parameters
  ----------
  im
      The AxesImage to be labeled.
  data
      Data used to annotate.  If None, the image's data is used.  Optional.
  valfmt
      The format of the annotations inside the heatmap.  This should either
      use the string format method, e.g. "$ {x:.2f}", or be a
      `matplotlib.ticker.Formatter`.  Optional.
  textcolors
      A pair of colors.  The first is used for values below a threshold,
      the second for those above.  Optional.
  threshold
      Value in data units according to which the colors from textcolors are
      applied.  If None (the default) uses the middle of the colormap as
      separation.  Optional.
  **kwargs
      All other arguments are forwarded to each call to `text` used to create
      the text labels.

  Notes
  -----
      For the text kwargs:
      - use 'size' to change the text size within the boxes
  """

  if not isinstance(data, (list, np.ndarray)):
    data = im.get_array()

  # Normalize the threshold to the images color range.
  if threshold is not None:
    threshold = im.norm(threshold)
  else:
    threshold = im.norm(data.max())/2.

  # Set default alignment to center, but allow it to be
  # overwritten by textkw.
  kw = dict(horizontalalignment="center",
            verticalalignment="center")
  kw.update(textkw)

  # Get the formatter in case a string is supplied
  if isinstance(valfmt, str):
    valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)   #type: ignore

  # Loop over the data and create a `Text` for each "pixel".
  # Change the text's color depending on the data.
  texts = []
  for i in range(data.shape[0]):
    for j in range(data.shape[1]):
      kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
      text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
      texts.append(text)

  return texts

    
def plotBinned(l, numbins, filename='plot', v1=None, v2=None,
               c1='blue', c2='red', linecolor='black', barcolor='gray',
               xlabel='X axis', ylabel='Y axis', title='', caption=''):
  """
  Bin values from the given list and plot a histogram of the number per bin.

  @param l:        The list of values to plot.
  @param numbins:  The number of bins to use.
  @param filename: The name of the file to create.
  @param v1:       A value to mark with a vertical bar of color c1.
  @param v2:       A value to mark with a vertical bar of color c2.
  @param xlabel:   The label for the X axis.
  @param ylabel:   The label for the Y axis.
  @param title:    The title of the plot.
  """
  fig = plt.figure()
  ax = fig.add_subplot(111)
  if title:
    plt.title(title)

  if caption:
    tt = textwrap.fill(caption, width=70)
    plt.figtext(0.5, -0.03, tt, ha='center', va='top')

  # the histogram of the data
  (n, bins, patches) = ax.hist(l, numbins, facecolor=barcolor, alpha=1)

  # hist uses np.histogram under the hood to create 'n' and 'bins'.
  # np.histogram returns the bin edges, so there will be 50 probability
  # density values in n, 51 bin edges in bins and 50 patches.  To
  # everything lined up, we'll compute the bin centers
  #bincenters = 0.5*(bins[1:]+bins[:-1])
  ## add a 'best fit' line for the normal PDF
  #y = mlab.normpdf( bincenters, mu, sigma)
  #l = ax.plot(bincenters, y, 'r--', linewidth=1)

  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  ##ax.set_title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
  #ax.set_xlim(40, 160)
  #ax.set_ylim(0, 0.03)
  #ax.grid(True)

  color = linecolor
  plt.setp([ax.get_xticklines(), ax.get_yticklines()], color=color) #type: ignore
  ax.spines['bottom'].set_color(color)
  ax.spines['top'].set_color(color)
  ax.spines['left'].set_color(color)
  ax.spines['right'].set_color(color)
  ax.xaxis.label.set_color(color)
  ax.yaxis.label.set_color(color)
  ax.tick_params(axis='x', colors=color)
  ax.tick_params(axis='y', colors=color)
  ax.patch.set_facecolor(color)
  ax.patch.set_alpha(0.8)

  if v1 != None:
    ax.axvline(x=v1, linewidth=4, color=c1)
  if v2 != None:
    ax.axvline(x=v2, linewidth=4, color=c2, alpha=0.75)

  #plt.show()
  plt.savefig(filename+".pdf", transparent=True, bbox_inches='tight')
  plt.close()



def plotBinned2(l1, l2, numbins, filename='plot', v1=None, v2=None,
                c1='blue', c2='red', linecolor='black', barcolor1='gray',
                barcolor2='white', xlabel='X axis', ylabel='Y axis', title='',
                caption=''):
  """
  Bin values from the given lists and plot two histograms.

  @param l1:       The first list of values to plot.
  @param l2:       The second list of values to plot.
  @param numbins:  The number of bins to use.
  @param filename: The name of the file to create.
  @param v1:       A value to mark with a vertical bar of color c1.
  @param v2:       A value to mark with a vertical bar of color c2.
  @param xlabel:   The label for the X axis.
  @param ylabel:   The label for the Y axis.
  @param title:    The title of the plot.
  """
  fig = plt.figure()
  ax = fig.add_subplot(111)
  if title:
    plt.title(title)

  if caption:
    tt = textwrap.fill(caption, width=70)
    plt.figtext(0.5, -0.03, tt, ha='center', va='top')
    #plt.figtext(0.5, -0.03, caption, wrap=True, horizontalalignment='center', fontsize=10)

  if l1 or l2:
    minval = min(itertools.chain(l1,l2))
    maxval = max(itertools.chain(l1,l2))
    #binedges = np.linspace(minval, maxval, numbins)
    #(n, bins, patches) = ax.hist(l1, binedges, facecolor=barcolor1, alpha=1,
    #                             edgecolor=linecolor, density=False)
    #(n, bins, patches) = ax.hist(l2, binedges, facecolor=barcolor2, alpha=0.75,
    #                             edgecolor=linecolor, density=False)
    (n, bins, patches) = ax.hist(l1, numbins, facecolor=barcolor1, alpha=1,
                                 edgecolor=linecolor, density=True)
    (n, bins, patches) = ax.hist(l2, numbins, facecolor=barcolor2, alpha=0.75,
                                 edgecolor=linecolor, density=True)

  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)

  color = linecolor
  plt.setp([ax.get_xticklines(), ax.get_yticklines()], color=color) #type: ignore
  ax.spines['bottom'].set_color(color)
  ax.spines['top'].set_color(color)
  ax.spines['left'].set_color(color)
  ax.spines['right'].set_color(color)
  ax.xaxis.label.set_color(color)
  ax.yaxis.label.set_color(color)
  ax.tick_params(axis='x', colors=color)
  ax.tick_params(axis='y', colors=color)
  ax.patch.set_facecolor(color)
  ax.patch.set_alpha(0.8)

  if v1 != None:
    ax.axvline(x=v1, linewidth=4, color=c1, alpha=0.75)
  if v2 != None:
    ax.axvline(x=v2, linewidth=4, color=c2, alpha=0.75)


  #plt.show()
  plt.savefig(filename+".pdf", transparent=True, bbox_inches='tight')
  plt.close()


def plotBinnedLines(l, numbins, values, colors, filename='plot',
                    linecolor='black', barcolor='gray',
                    xlabel='X axis', ylabel='Y axis',
                    title='', caption=''):
  """
  Bin values from the given list and plot them as an histograms, then
  add a line of the given color c for each corresponding value v.

  @warning: It is assumed that v and c have the same length.

  @param l:       The list of values to bin and plot.
  @param numbins:  The number of bins to use.
  @param filename: The name of the file to create.
  @param values:   The list of values to plot as vertical lines.
  @param colors:   The colors corresponding to values v.
  @param xlabel:   The label for the X axis.
  @param ylabel:   The label for the Y axis.
  @param title:    The title of the plot.
  """
  fig = plt.figure()
  ax = fig.add_subplot(111)
  if title:
    plt.title(title)

  if caption:
    tt = textwrap.fill(caption, width=70)
    plt.figtext(0.5, -0.03, tt, ha='center', va='top')

  # the histogram of the data
  (n, bins, patches) = ax.hist(l, numbins, facecolor=barcolor, alpha=1)

  # hist uses np.histogram under the hood to create 'n' and 'bins'.
  # np.histogram returns the bin edges, so there will be 50 probability
  # density values in n, 51 bin edges in bins and 50 patches.  To
  # everything lined up, we'll compute the bin centers
  #bincenters = 0.5*(bins[1:]+bins[:-1])
  ## add a 'best fit' line for the normal PDF
  #y = mlab.normpdf( bincenters, mu, sigma)
  #l = ax.plot(bincenters, y, 'r--', linewidth=1)

  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  ##ax.set_title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
  #ax.set_xlim(40, 160)
  #ax.set_ylim(0, 0.03)
  #ax.grid(True)

  color = linecolor
  plt.setp([ax.get_xticklines(), ax.get_yticklines()], color=color) #type: ignore
  ax.spines['bottom'].set_color(color)
  ax.spines['top'].set_color(color)
  ax.spines['left'].set_color(color)
  ax.spines['right'].set_color(color)
  ax.xaxis.label.set_color(color)
  ax.yaxis.label.set_color(color)
  ax.tick_params(axis='x', colors=color)
  ax.tick_params(axis='y', colors=color)
  ax.patch.set_facecolor(color)
  ax.patch.set_alpha(0.8)

  for v, c in zip(values, colors):
    ax.axvline(x=v, linewidth=4, color=c, alpha=0.75)

  #plt.show()
  plt.savefig(filename+".pdf", transparent=True, bbox_inches='tight')
  plt.close()



def plotLinear(filename, data, spot=None, xlabel='', ylabel=''):
  """
  Plot the x,y data given in data.  Make a mark at spot.

  @param filename: the name of the plot
  @param data:     the x,y data
  @param spot:     the spot to mark with a red bar
  @param xlabel:   the x axis label
  @param ylabel:   the y axis label
  """
  fig = plt.figure()
  ax = fig.add_subplot(111)

  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)

  if spot:
    ax.axvline(x=spot, linewidth=4, color='red', alpha=0.25)

  ax.plot(*list(zip(*data)))

  #plt.show()
  plt.savefig(filename+".pdf", transparent=True, bbox_inches='tight')
