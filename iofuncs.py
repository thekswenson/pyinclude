# Krister Swenson                                              Winter 2012-13
"""
 Convenience function for I/O.
"""

from typing import Iterable, List
from pathlib import Path


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#



def print2DMatrix(m: Iterable[Iterable], cellwidth = 10):
  """
  Print the 2D matrix in a nice format.
  """
  for row in m:
    for e in row:
      print(str(e).ljust(cellwidth), end=' ')
    print()


############ Types to give argparse:

def commaSepFloats(s: str) -> List[float]:
  """
  Convert a string of comma-separated floats into a list of floats.
  """
  return [float(x) for x in s.split(',')]


def commaSepInts(s: str) -> List[int]:
  """
  Convert a string of comma-separated ints into a list of ints.
  """
  return [int(x) for x in s.split(',')]


def newPath(path: str) -> Path:
  """
  Convert a string into a Path object.
  """
  p = Path(path)
  if p.exists():
    raise ValueError('Path exist: {p}.')

  return p
