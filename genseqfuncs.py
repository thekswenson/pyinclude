#!/usr/bin/python
# Krister Swenson
#
"""
Functions for working with genome sequences.
"""
import subprocess
import re

from pathlib import Path
from typing import Dict, Iterable, List, Optional, Set, Tuple
from collections import defaultdict as dd

from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord
from Bio.SearchIO import Hit


### Constants:



def sliceFasta(infile, outfile, accTOinterval):
  """
  Take a slice of all the sequences in the given fasta file and write it to
  a new outfile.

  Only include the genomes that are keys in accTOinterval.  If the values of
  accTOinterval are interval pairs, then slice these pairs.

  @param infile:        the input fasta file
  @param outfile:       the output fasta file
  @param accTOinterval: map accession number (or anything in the description)
                        to the interval to keep.  The None interval implies that
                        the entire string is kept.
  """
  writerecs = []
  headerre = re.compile(r'(.+)\|:\d+-\d+(.*)')
  for r in SeqIO.parse(infile, 'fasta'):
    found = None
    for acc in accTOinterval:
      if re.search(acc, r.description):
        found = acc
        break

    if found:
      if accTOinterval[acc]:    #If there is an interval.
        start,end = accTOinterval[acc]
        r = r[start:end]

        #idstr, descstr = r.description.rsplit('|',1)
        m = headerre.match(r.description)
        if m:
          r.id = '{}|:{}-{}'.format(m.group(1),start,end)
          r.description = m.group(2)

      writerecs.append(r)

  print(writerecs)
  SeqIO.write(writerecs, outfile, "fasta")



def sliceFastaFile(infile: Path | str, outfile: Path | str,
                   coordinates: Optional[List[Tuple[int, int]] |
                                         Tuple[int, int]]=None,
                   whitelist: List[str]=[],
                   blacklist: List[str]=[],
                   includelines: List[int]=[]) -> None:
  """
  Take a slice of all the sequences in the given fasta file and write it to
  a new outfile.

  @note: If coordinate pair is None for a given index, then the complete
         sequences are used.

  @note: None can be used to denote the begining or end of the sequence.  For
         example, (4,None) will take index 4 to the end of the sequence.

  @note: If the first coordinate is greater than the second, then slice the
         rotated sequence (i.e. concatinate the beginning to the end).

  @param infile:      the input fasta file
  @param outfile:     the output fasta file
  @param coordinates: (start, end) tuple or list of such pairs. If None, 
                      then include the entire genomes.  If one of the
                      two is None, then go to the end of the sequence.
                      If a list, any missing coordinates (the length of
                      coordinates is not equal to the number of genomes 
                      considered) will default to the empty genome.
                      If start is after end, then include the begining and
                      end (as though the sequence wraps).
  @param whitelist:   keep these genomes
  @param blacklist:   remove these genomes
  @param includelines:only include genomes from these lines (e.g. [2,4] means
                      to include the second and fourth genomes in the file).
  """
  writerecs = selectFastaRecords(infile, coordinates, whitelist,
                                 blacklist, includelines)

  SeqIO.write(writerecs, outfile, "fasta")


def selectFastaRecords(infile: Path | str,
                       coordinates: Optional[List[Tuple[int, int]] |
                                             Tuple[int, int]]=None,
                       whitelist: List[str]=[],
                       blacklist: List[str]=[],
                       includelines: List[int]=[]) -> List[SeqRecord]:
  """
  Select matching records according to the given parameters.

  Parameters
  ----------
  infile : Path
      fasta file
  coordinates : Optional[List[Tuple[int, int]] | Tuple[int, int]], optional
      (start, end) tuple or list of such pairs. If None, 
      then include the entire genomes.  If one of the
      two is None, then go to the end of the sequence.
      If a list, any missing coordinates (the length of
      coordinates list is not equal to the number of genomes 
      considered) will default to the empty genome.
      If start is after end, then include the begining and
      end (as though the sequence wraps circularly).
  whitelist : list, optional
      keep genomes that match one of these RE patterns, by default []
  blacklist : list, optional
      reject genomes that match one of these RE patterns, by default []
  includelines : _type_, optional
      only include genomes from these lines (e.g. [2,4] means
      to include the second and fourth genomes in the file).

  Returns
  -------
  List[SeqRecord]
      Matching SeqRecords.
  """
  writerecs = []
  for i, r in enumerate(SeqIO.parse(infile, 'fasta')):
    if((not includelines or
        i in includelines) and
       (not whitelist or
        any(re.search(w, r.description) for w in whitelist)) and
       (not blacklist or
        not any(re.search(w, r.description) for w in blacklist)) and
       not undetermined(r)):

          #Set the coordinates if they're specified:
      if not coordinates:
        start, end = 0, None

      elif type(coordinates) == list:
        if i >= len(coordinates):
          start = end = None
        elif coordinates[i]:
          start, end = coordinates[i]
        else:
          start, end = 0, None

      else:
        start, end = coordinates

          #Slice the sequence:
      newr = r
      if start == None and end == None:
        newr.seq = Seq.Seq('')
      else:
        if start == None:
          newr = r[:end]
          start = 1
        elif end == None:
          newr = r[start:]
          end = len(r)
        elif start <= end:
          newr = r[start:end]
        else:
          newr = r[start:]+r[:end]

          #Construct the new id line:
      assert end != None and start != None
      if end - start < len(r.seq):
        if '|' in newr.description:
          idstr, descstr = newr.description.rsplit('|',1)
        else:
          idstr, descstr = newr.description, ''
        newr.id = '{}|:{}-{}'.format(idstr, start, end)
        newr.description = descstr

      removeDigits(newr)

      writerecs.append(newr)

  return writerecs


def getFastaDescriptions(fastafiles: Iterable[Path])\
  -> List[Tuple[str, str, Dict[str, str]]]:
  """
  Return the elements of descriptions from the given fasta file.
  """
  retlist = []
  for fastafile in fastafiles:
    with open(fastafile) as f:
      for line in f:
        if line.startswith('>'):
          retlist.append(parseFastaDescription(line[1:].strip()))

  return retlist

  #return [parseFastaDescription(r.description)
  #        for fastafile in fastafiles
  #        for r in SeqIO.parse(fastafile, 'fasta')]


def parseFastaDescription(descstr: str)\
  -> Tuple[str, str, Dict[str, str]]:
  """
  Extract information from a description line in a fasta file according to the
  format described at GenBank
  (https://www.ncbi.nlm.nih.gov/genbank/fastaformat/)

  Return
  ------
  seqid: str
    The dataset name.
  desc: str
    The description.
  modifiers: Dict[str, str]
    A dictionary of modifiers (e.g. organism, strain, chromosome, etc. which
    can be found at https://www.ncbi.nlm.nih.gov/genbank/mods_fastadefline). 
  """
  #descre = re.compile(r'>(\S+) (\[(\S+)=(\S*)\] *)*')
  keyvalre = re.compile(r'\[(\S+?)=(\S*?)\]\s*')

  modifiers: Dict[str, str] = {}
  origdescstr = descstr
  while m := keyvalre.search(descstr):
    modifiers[m.group(1)] = m.group(2)
    descstr = keyvalre.sub('', descstr, count=1)

  if m := re.match(r'(\S+)\s+(.*)', descstr):
    seqid, desc = m.group(1), m.group(2)
  else:
    raise(Exception('Malformed description: "{}"'.format(origdescstr)))
    
  return seqid, desc, modifiers


genbankre = re.compile(r'([A-Z]+\d+)\.?\d*')
def isAccessionGenBank(accstr: str) -> str:
  """
  If the given string is a valid accession number, return the part before the
  decimal point. Otherwise, return the empty string.
  """
  if m := genbankre.match(accstr):
    return m.group(1)

  return ''


def getChromInfo(fastafiles: Iterable[Path]) -> Dict[str, Dict[str, str]]:
  """
  Return information from the description lines in the given fasta file.

  Return
  ------
  spec2seqid2chrom: Dict[str, Dict[str, str]]
    Map species to a dictionary of sequence ids to chromosome name
  """
  spec2seqid2chrom: Dict[str, Dict[str, str]] = dd(dict)
  chromre = re.compile(r'chromosome (\w+)', re.IGNORECASE)
  for seqid, desc, modifiers in getFastaDescriptions(fastafiles):
    if 'species' in modifiers:
      species = modifiers['species']
    else:
      species = ' '.join(desc.split()[:2])  #take first two words of description

    if 'chromosome' in modifiers:
      chrom = modifiers['chromosome']
    else:
      if m := chromre.search(desc):
        chrom = m.group(1)
      else:
        raise(Exception(f'Could not find chromosome in "{desc}"!'))

    spec2seqid2chrom[species][seqid] = chrom
    
  return spec2seqid2chrom


def removeDigits(record):
  """
  Replace digits with gaps in the given sequence.
  """
  newseq = Seq.Seq('')
  for char in record.seq:
    if char.isdigit():
      newseq += '-'
    else:
      newseq += char

  record.seq = newseq


def undetermined(record):
  """
  Return True if the sequence has only undetermined characters (e.g. x,-,=).
  """
  udchars = set(['-', '=', 'x'])
  for char in record.seq:
    if char not in udchars:
      return False

  return True


def printRecordDescriptors(fastafile):
  """
  Print to stdout the descriptors in the given fasta file.
  """
  print('\n'.join('{}: {}'.format(i, r.description)
                  for i,r in enumerate(SeqIO.parse(fastafile, 'fasta'))))


def getSeqLengthsMULTI(seqfiles: List[Path]):
  """
  Return the sequence lengths from the given files.
  """
  id2len = {}
  for seqfile in seqfiles:
    id2len.update(getSeqLengths(seqfile))
    
  return id2len


def getSeqLengths(seqfile: Path,
                  idTOi: Optional[Dict[str, int]]=None,
                  sidset: Optional[Set]=None) -> Dict[str, int]:
  """
  Return the sequence lengths from the given file.

  Parameters
  ----------
  seqfile: Path
    The fasta file.
  idTOi: Dict[str, int], optional
    Map from seqid to order in the fasta file.
  gidset: Set, optional
    Restrict the mapping to these seqids.

  @return: map from sid to genome length
  """
  if idTOi:
    lenlist = []
    for r in SeqIO.parse(seqfile, 'fasta'):
      lenlist.append(len(r))
    return {sid: lenlist[idTOi[sid]] for sid in idTOi.keys()
            if not sidset or sid in sidset}
  else:
    return {r.id: len(r) for r in SeqIO.parse(seqfile, 'fasta')
            if not sidset or r.id in sidset}


def makeBlastDB(infasta: Path, outpref: Path):
  """
  Make the database files for BLAST searchs.

  Parameters
  ----------
  outpref : Path
      the output file path prefix (can be a directory)
  """
  subprocess.run(['makeblastdb', '-in', infasta, '-dbtype nucl',
                  '-parse_seqids', '-out', outpref])


def runBlastN(query: Path, db: Path, results: Path, threads: int,
              wordsize=11, reward=2, penalty=-3, gapopen=5, gapextend=2):
  """
  Run the locally installed bastn (`sudo apt install ncbi-blast+`).
  (parse results with SearchIO.read(results, 'blast-xml'))

  Parameters
  ----------
  query : Path
      the query fasta file
  db : Path
      the database file path prefix (output of makeblastdb)
  results : Path
      the results in this XML file
  threads : int
      use this many threads to run blastn
  """
  subprocess.run(['blastn', '-query', str(query), '-db', str(db), '-out',
                  str(results), '-word_size', str(wordsize),
                  '-reward', str(reward),
                  '-penalty', str(penalty),
                  '-gapopen', str(gapopen),
                  '-gapextend', str(gapextend),
                  '-num_threads', str(threads), '-outfmt', '5'],
                 check=True)


def runTBlastN(query: Path, db: Path, results: Path, threads: int,
               wordsize=3, gapopen=11, gapextend=1):
  """
  Run the locally installed tblastn (`sudo apt install ncbi-blast+`).
  (parse results with SearchIO.read(results, 'blast-xml'))

  Parameters
  ----------
  query : Path
      the query fasta file containing a protein sequence
  db : Path
      the nucleotide database file path prefix (output of makeblastdb)
  results : Path
      the results in this XML file
  threads : int
      use this many threads to run blastn
  """
  subprocess.run(['tblastn', '-query', str(query), '-db', str(db), '-out',
                  str(results), '-word_size', str(wordsize),
                  '-gapopen', str(gapopen),
                  '-gapextend', str(gapextend),
                  '-num_threads', str(threads), '-outfmt', '5'],
                 check=True)

def runBlastNonFile(query: Path, subject: Path, results: Path,
                    wordsize=11, reward=2, penalty=-3, gapopen=5, gapextend=2):
  """
  Run the locally installed bastn (`sudo apt install ncbi-blast+`).
  (parse results with SearchIO.read(results, 'blast-xml'))

  Notes
  -----
  From the docs: The (absolute) reward/penalty ratio should be increased as one
  looks at more divergent sequences. A ratio of 0.33 (1/-3) is appropriate for
  sequences that are about 99% conserved; a ratio of 0.5 (1/-2) is best for
  sequences that are 95% conserved; a ratio of about one (1/-1) is best for
  sequences that are 75% conserved.

  Parameters
  ----------
  query : Path
      the query fasta file
  target : Path
      the fasta file to search in
  results : Path
      put the results in this XML file
  threads : int
      use this many threads to run blastn
  """
  subprocess.run(['blastn', '-query', str(query), '-subject', str(subject),
                  '-out', str(results), '-word_size', str(wordsize),
                  '-reward', str(reward),
                  '-penalty', str(penalty),
                  '-gapopen', str(gapopen),
                  '-gapextend', str(gapextend),
                  '-outfmt', '5'],
                 check=True)


def getBlastHitAccession(hit: Hit) -> str:
  """
  Return the accession number of the hit in the given blast record.
  """
  haccession = isAccessionGenBank(hit.accession)
  if not haccession:
    haccession = isAccessionGenBank(hit.id)
  if not haccession:
    raise(ValueError(f'Both hit.accession and hit.id are not accessions:\n'
                     f' {hit.accession} {hit.id}'))
 
  return haccession
