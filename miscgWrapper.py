#Krister Swenson                                               Winter 2013-14
"""
Functions for taking the maximum independent set of circle graphs.
  ** this is from the paper "A New Simple Algorithm for the Maximum-Weight
     Independent Set Problem on Circle Graphs" by Valiente.
  ** It runs in O(dn) time where d is the maximum number of intervals
     at a particular index.

This is a python interface to the C code.
You need to build the shared library:
g++ -shared -Wl,-soname,libmiscg.so -o libmiscg.so -fPIC miscg.C
"""
import ctypes

libc = ctypes.CDLL('libc.so.6')
libmis = ctypes.CDLL('/home/kms/home/phylo/thedist/tools/libmiscg.so')

def misCG(dow):
  """
  Take the double occurance word and compute the MIS on it.

  @param dow: the word
  """
  print(dow)
  print(libmis)
  #print libmis.getFunc(2)
  print(libc.time(None))
  print(libc.printf)
  print('________________')
  print(libmis.getMISCG)
  print('________________')

  retval = None
  libmis.getMISCG(dow, retval)
  print(retval)
