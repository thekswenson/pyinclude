# Krister Swenson                                                Winter 2013-14
#
"""
Functions for working with and reconstructing phylogenetic trees.
"""
import os,re,sys
import subprocess

from Bio import AlignIO
from Bio.Phylo.Applications import PhymlCommandline
from Bio.Emboss.Applications import FDNAParsCommandline
from Bio.Align import MultipleSeqAlignment
from Bio.Alphabet import IUPAC, Gapped

import dendropy     #For running RAxML.
from dendropy.interop import raxml


### Constants:

RAXML = '/home/kms/home/phylo/raxml-devel/raxmlHPC-SSE3'

PHYML_TREE_EXT = '.phy_phyml_tree.txt'
RAXML_TREE_EXT = '.raxml.nwk'
PARS_TREE_EXT = '.parsimony.nwk'

GAP_CHAR = '-'



#_______________________________________________________________________________
#___________ TREE INFERENCE ____________________________________________________



def inferTreeOnInterval(alignment, start, end, useraxml=False, dirname='.',
                        whitelist=[], blacklist=[], reference=None, digits=0,
                        useparsimony=False):
  """
  Infer the tree on the given interval.  Leave it in a file.

  @note: Creates a directory and store tree and alignment files there.

  @param alignment: the multiple alignment.
  @param start:     the start of the interval.
  @param end:       the end of the interval.
  @param useraxml:  True if we are to use RAxML instead of phyML.
  @param useparsimony:  True if we are to use phylip's parsimony.
  @param dirname:   the directory name to put the files in.
  @param whitelist: names of the only taxa to keep.
  @param blacklist: names of taxa to remove.
  @param reference: the name of the reference sequence to index on (e.g. HXB2).
  @param digits:    minimum number of digits for index in the filenames

  @return: (subalign, treefilename) pair or (None, None) if that interval has
           a row that is more than 40% gaps.
  @rtype:  subalign is the sub-alignment object and treefilename is a string.
  """
    #Get a map of indices from the alignment to HXB2:
  remappedstr = ''
  startstr = str(start).zfill(digits)
  endstr = str(end).zfill(digits)
  if reference:
    remappedstr = '.remapped'
    aiTOi, iTOai = getIndexMap(alignment, reference)
    start = iTOai[start]
    end = iTOai[end]
    print('using alignment interval '+str((start, end))+'.')

    #Choose the correct rows and columns:
  subalign = getSubAlignment(alignment, start, end, whitelist, blacklist)

  if not enoughDataIn(subalign):
    print('skipping due to sparse alignment.')
    return (None, None)

    #Build the directory names and file names:
  if not os.path.exists(dirname):
    os.mkdir(dirname)
  whitestr=''
  if whitelist:
    whitestr = '.'+'.'.join(whitelist)
  blackstr=''
  if blacklist:
    blackstr = '_'+'.'.join(blacklist)
  fbasename = dirname+remappedstr+whitestr+blackstr+'.'+startstr+'-'+endstr
  treepathbase = dirname+'/'+fbasename

    #Reconstruct the tree:
  if useraxml:
    treefilename = treepathbase+RAXML_TREE_EXT
    if os.path.exists(treefilename):
      print('tree "'+treefilename+'" already exists...')
    else:
      print("running RAxML...")
      subalignfile = treepathbase+'.nexus'
      AlignIO.write(subalign, subalignfile, 'nexus')
      data = dendropy.DnaCharacterMatrix.get_from_path(subalignfile, 'nexus')
      rx = raxml.RaxmlRunner()
      tree = rx.estimate_tree(data)
      tree.write_to_path(treefilename, 'newick', suppress_rooting=True)
  elif useparsimony:
    treefilename = treepathbase+PARS_TREE_EXT
    if os.path.exists(treefilename):
      print('tree "'+treefilename+'" already exists...')
    else:
      print("running fdanpars...")
      subalignfile = treepathbase+'.phy'
      AlignIO.write(subalign, subalignfile, 'phylip')
      cmd = FDNAParsCommandline(sequence=subalignfile, outfile='/dev/null',
                                intreefile='', outtreefile=treefilename)
      out_log, err_log = cmd()
  else:
    treefilename = treepathbase+PHYML_TREE_EXT
    if os.path.exists(treefilename):
      print('tree "'+treefilename+'" already exists...')
    else:
      print("running phyML...")
      subalignfile = treepathbase+'.phy'
      AlignIO.write(subalign, subalignfile, 'phylip-relaxed')
      cmd = PhymlCommandline(input=subalignfile)
      out_log, err_log = cmd()

  return subalign, treefilename



def getIndexMap(alignment, reference='HXB2'):
  """
  Get a map of indices from the alignment to a given reference.
  """
      #Get the line with the reference:
  refrow = None
  for row in alignment:
    if row.id == reference:
      refrow = row.seq
      break
  if not refrow:
    sys.exit('ERROR: '+reference+' must be in the alignment to index off it!')

  return buildIndexMap(refrow)

def buildIndexMap(line, gapchar='-'):
  """
  iTOa[i] will be the index of the ith character in the line for the
  gapless version sequence.
  aTOi[i] will be the inverse (it's not a function so an arbitrary inverse
  will be used in case of multiple).
  """
  iTOa = []
  aTOi = []
  for c in line:                   #For each character in the query line:
    if len(iTOa):
      last = iTOa[-1]
    else:
      last = 0
    if c == gapchar:              #if it's a gap,
      iTOa.append(last)            #then don't increment the index.
    else:
      aTOi.append(len(iTOa)-1)
      iTOa.append(last+1)          #Otherwise, increment.

  return iTOa, aTOi


def getSubAlignment(fullalign, start, end, whitelist, blacklist):
  """
  Extract the rows from the alignment in the whitelist.
  Extract the columns from start to end.
  """
  alignment = MultipleSeqAlignment([], alphabet=Gapped(IUPAC.ambiguous_dna))
  for i in range(len(fullalign)):
    #fullalign[i].id.split('.')[1].split('_')[0] in whitelist:
    foundwhite = False
    if whitelist:
      for w in whitelist:
        if re.search(w, fullalign[i].id):
          foundwhite = True
          break
    foundblack = False
    if blacklist:
      for w in blacklist:
        if re.search(w, fullalign[i].id):
          foundblack = True
          break

    if((not whitelist and not blacklist) or (whitelist and foundwhite) or
       (blacklist and not foundblack)):
      alignment.extend([fullalign[i, start:end]])
  return alignment


def enoughDataIn(alignment, sigratio=0.6):
  """
  Return True if all the lines in the given alignment have a significant number
  of non-gap characters (i.e. they have more than sigratio proportion of
  non-gaps)
  """
  for row in alignment:
    count = 0
    for i in row:
      if i != GAP_CHAR:
        count += 1

    if float(count)/len(row) < sigratio:
      return False

  return True



#___________ TREE INFERENCE ____________________________________________________
#_______________________________________________________________________________




#_______________________________________________________________________________
#___________ TREE CONSENSUS  ___________________________________________________



def computeAST(trees, kast=True, mast=False, allmasts=False,
               verbose=False, suffix='', dirname=None, cleanup=True):
  """
  Compute the MAST or KAST or all MASTs on the given list of trees.

  @note: this is a wrapper for the modified RAxML code.

  @param trees:    the list of trees where str() returns a NH format string
  @param kast:     compute the kast
  @param mast:     compute the mast
  @param allmasts: compute all masts
  @param verbose:  output to stdout
  @param suffix:   the suffix to add to the files used by the C code
  @param dirname:  the directory to run stuff in
  @param cleanup:  remove the temporary files
  """
  suffix += '.txt'
  treefile = '.trees.tmp.nwk'
  KASTPREFIX = "RAxML_KernelAgreementSubtree"
  MASTPREFIX = "RAxML_MaximumAgreementSubtree"
  INFOPREFIX = "RAxML_info"
  KAST_FILE = KASTPREFIX+"."+str(suffix)
  MAST_FILE = MASTPREFIX+"."+str(suffix)
  INFO_FILE = INFOPREFIX+"."+str(suffix)

  cwd = os.getcwd()
  if dirname:
    os.chdir(dirname)

  writeTreeList(trees, treefile);    #Write the trees to a temp file.

                                     #Find the KAST:
  if verbose:
    if kast:
      subprocess.call([RAXML,"-JKAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"])
      os.unlink(INFO_FILE)

    if allmasts:                     #Find all the MASTs:
      subprocess.call([RAXML,"-JALL_MAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"])
      os.unlink(INFO_FILE)
    elif mast:
      subprocess.call([RAXML,"-JMAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"])
      os.unlink(INFO_FILE)
  else:
    fnull = open(os.devnull, 'w')     #(this is just to supress output)
    if kast:
      subprocess.call([RAXML,"-JKAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"], stdout = fnull,
                      stderr = fnull)
      os.unlink(INFO_FILE)

    if allmasts:                     #Find all the MASTs:
      subprocess.call([RAXML,"-JALL_MAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"], stdout = fnull,
                      stderr = fnull)
      os.unlink(INFO_FILE)
    elif mast:
      subprocess.call([RAXML,"-JMAST","-z"+treefile,
                      "-n"+str(suffix),"-mGTRCAT"], stdout = fnull,
                      stderr = fnull)
      os.unlink(INFO_FILE)
    fnull.close()



  kastsize = 0
  mastsize = 0
  mastnumber = 0
  if kast:
    kastsize = getSizeOfTreeInFile(KAST_FILE)
    if cleanup:
      os.unlink(KAST_FILE)
  if allmasts:
    mastsize = getSizeOfTreeInFile(MAST_FILE)
    mastnumber = getNumberOfTrees(MAST_FILE)
    if cleanup:
      os.unlink(MAST_FILE)
  elif mast:
    mastsize = getSizeOfTreeInFile(MAST_FILE)
    if cleanup:
      os.unlink(MAST_FILE)

  os.unlink(treefile)

  if dirname:
    os.chdir(cwd)

  return (kastsize,mastsize,mastnumber)






def writeTreeList(trees, filename):
  """
  Write all the trees of -alltrees- to a file.

  @note:    Should be reimplemented with biopython.
  """

  f = open(filename, 'w')           #Open the file.
  for tree in trees:
    f.write(getCleanNH(str(tree))+"\n")
  f.close()


def getCleanNH(t):
  "Return the NH string for the given tree without edge lengths."
  return re.sub(r":[\w.-]+", r"", str(t))+";"


def getSizeOfTreeInFile(file):
  """
  Return the size of the first tree in newick (NH) format in the file.

  @warning: it must be on the first line!
  """
  f = open(file, 'r')
  tree = f.read()
  f.close()

  return getSizeOfTree(tree)


def getSizeOfTree(tree):
  """
  Return the size of the first tree in newick (NH) format in the file.

  @note:    Should be reimplemented with biopython.
  """
  tree = re.sub(r";[\W\S]*",";",tree,1)   #Remove the other trees.
  (newone, n) = re.subn(r"[,;]","",tree)  #Count the commas.
  if n:
    return n
  return 0


def getNumberOfTrees(file):
  """
  Return the number of trees in newick (NH) format in the file.

  @note:    Should be reimplemented with biopython.
  """
  f = open(file, 'r')
  tree = f.read()
  f.close()

  (newone, n) = re.subn(r";",";",tree)  #Count the semicolons.
  return n




#___________ TREE CONSENSUS  ___________________________________________________
#_______________________________________________________________________________
