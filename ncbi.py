# Krister Swenson                                                  Fall 2012
#
# Library for getting info from NCBI using Biopython entrez.
#


import os
import re
import time
from typing import Callable, Dict, List
from pathlib import Path

from Bio import Entrez, SeqIO
from Bio.SeqRecord import SeqRecord
from .usefulfuncs import printnow, printerr

# Constansts:

REQUEST_TIME = 2.0   #The time to wait between NCBI requests.


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

class EmptyEntrezSearchError(Exception):
  pass

class InvalidAccessionError(Exception):
  def __init__(self, value):
    super().__init__()
    self.value = value

  def __str__(self):
    return self.value

#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

def fetchGBRecord(accession: str, accTOid: Dict[str, str] = None,
                  gbdir='.', filetype='gb') -> SeqRecord:
  """
  Single accession version of `fetchGBRecords`.
  """
  gid = accession
  if accTOid and accession in accTOid:
    gid = accTOid[accession]

  return fetchGBRecords([accession], accTOid, gbdir, filetype)[gid]

def fetchGBRecords(accessions: List[str], accTOid: Dict[str, str] = None,
                   gbdir='.', filetype='gb') -> Dict[str, SeqRecord]:
  """
  Get a dictionary mapping id to record from NCBI genbank or files if they
  exist.

  Notes
  -----
    always do Entrez.email = "A.N.Other@example.com" before using this!

  Parameters
  ----------
  accessions : List[str]
      list of accessions
  accTOid : Dict[str, str], optional
      maps accession to genome id, by default None
  gbdir : str, optional
      the location to store/fetch the gb files. if the directory does not exist
      then we store the files here, by default '.'
  filetype : str, optional
      the type of file to store/fetch, by default 'gb'

  Returns
  -------
  Dict[str, SeqRecord]
      map from gid to genbank record if `accTOid` is defined, otherwise a
      map from acc to genbank record.
  """
  assert(Entrez.email)

  idTOrec = {}
  if os.path.exists(gbdir):
    if not os.path.isdir(gbdir):
      raise(Exception(f'ERROR: "{gbdir}" is not a directory. Rename it.'))
  else:
    os.makedirs(gbdir)

  for acc in accessions:
    filename = Path(gbdir+'/'+acc+'.gb')

    if not filename.exists():
      printnow(f'Creating {filename}...')
      handle = Entrez.read(Entrez.esearch(db="nucleotide", term=acc,
                           retmode="text"))
      if handle['Count'] == '0':
        raise(EmptyEntrezSearchError(f'"{acc}" not found.'))
      genomeID = handle['IdList'][0]

      record = Entrez.efetch(db="nucleotide", id=genomeID, rettype="gb",
                             retmode="text")
      val = record.read()

      with open(filename, 'w') as outfile:
        outfile.write(val)
      time.sleep(REQUEST_TIME)

    with open(filename) as f:
      record = SeqIO.read(f, "gb")

    if not accTOid:
      idTOrec[acc] = record

    elif record.id in accTOid:
      idTOrec[accTOid[record.id]] = record

    else:
      baserec = record.id.split('.')[0]
      if baserec in accTOid:
        idTOrec[accTOid[baserec]] = record
      else:
        raise(InvalidAccessionError(f'Invalid accession ID "{baserec}".'))

  return idTOrec


  #Accession format specified at: https://www.ncbi.nlm.nih.gov/Sequin/acc.html
accregex = re.compile(r'([A-Z][A-Z]\d{8}(\.\d+)?)|([A-Z][A-Z]_?\d{6}(\.\d+)?)|([A-Z]\d{5}(\.\d+)?)')
#accregex = re.compile(r'[A-Z][A-Z]?\d+\.?\d* ')
tagmark = set(['gb','ref','emb'])   #This indicates an accession is coming.
def accFromDescription(description, updatefunc: Callable=None):
  """
  From a description fasta line, extract the accession number.

  @updatefunc:  function used to send update messages to the user
  """
  tokens = description.split('|')
  i = 0
  for i,t in enumerate(tokens):        #Find the genbank identifier.
    if(t in tagmark):                  #If the next is an accession.
      break

  if(i < len(tokens)-1):
    acc = tokens[i+1]
  else: 
    firstword = description.split()[0]
    if(accregex.match(firstword)):
      acc = firstword     #The description has an accession as its first word.
    else:
      m = accregex.search(description)
      if(m):
        acc = m.group(0)  #The description has an accession hidden in it.
      else:
        message = 'WARN: description line "{}" '.format(description)+\
                  'has no genbank id.\n'+\
                  'Your fasta description line must have the string "ref|ACC"'+\
                  ' where ACC is\n the accession number of your genome.'
        if updatefunc: updatefunc(message)
        print(message)
        time.sleep(5)
        return None


  if(not accregex.match(acc)):
    message = f'WARN: fasta entry "{acc}" does not look like an accession.'
    if updatefunc: updatefunc(message)
    printerr(message)
    time.sleep(5)

  return acc


def nameFromDescription(description):
  """
  From a description fasta line, extract the name of the species.
  """
  if('|' in description):
    return description.split('|')[-1].partition(' ')[2]

  elif accregex.match(description):
    return ' '.join(description.split()[1:])

  else:
    return description.strip()
